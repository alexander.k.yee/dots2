" need to modify
"nnoremap <C-p> :call fzf#run( {'source':'python ~/.config/nvim/tools/NoteLister.py $(pwd)', 'sink' : 'e'})<CR>
"nnoremap <Leader>b :Buffers<CR>
"
"nnoremap <Leader>gd :call fzf#vim#tags(expand('<cword>'))<CR>
"
"nnoremap j gj
"nnoremap k gk
"
"" replaces z= with a hook to fzf instead of new buffer
"function! FzfSpellSink(word)
"  exe 'normal! "_ciw'.a:word
"endfunction
"function! FzfSpell()
"  let suggestions = spellsuggest(expand("<cword>"))
"  return fzf#run({'source': suggestions, 'sink': function("FzfSpellSink"), 'down': 10 })
"endfunction
"nnoremap z= :call FzfSpell()<CR>
"
"" creates a new file in pwd with specific format
"command! -nargs=1 NewZettel :execute ":e" . getcwd() . "/" . strftime("%Y%m%d%H%M") . "-<args>.md"
"
"nnoremap <leader>nz :NewZettel 
"function! ZettelFZF(file)
"    "let filename = fnameescape(fnamemodify(a:file, ":t"))
"    "why only the tail ?  I believe the whole filename must be linked unless everything is flat ...
"    let filename = fnameescape(a:file)
"    let filename_wo_timestamp = fnameescape(fnamemodify(a:file, ":t:s/^[0-9]*-//"))
"     " Insert the markdown link to the file in the current buffer
"    let mdlink = "[ ".filename_wo_timestamp." ]( ".filename." )"
"    put=mdlink
"endfunction
"
"command! -nargs=1 ZettelFZF :call ZettelFZF(<f-args>)
"
"" trigger backlink insert from insert mode
"inoremap <buffer> <C-L> <esc>:call fzf#run({'source':'python ~/.config/nvim/tools/NoteLister.py $(pwd)', 'sink': 'ZettelFZF'})<CR>
"
"" update tags
"nnoremap <leader>tt :silent !ctags -R . <CR>:redraw!<CR>
