syntax enable
setlocal expandtab
setlocal tabstop=4
setlocal shiftwidth=4
setlocal textwidth=80
setlocal smarttab
let &colorcolumn=join(range(79,80),",")
