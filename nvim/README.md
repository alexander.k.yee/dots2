# Debugging

If you have an existing nvim setup you might have issues since deoplete hardcodes a path in 
`$HOME/.local/share/nvim/rplugin.vim`

- wipe the file and then run `:UpdateRemotePlugins` to fix & generate a new rplugin.vim

Depending on how your system is set up, nvim cannot autoresolve $VIM & $VIMRUNTIME correctly

- try moving nvim binary to `/usr/bin` or `/usr/local/bin`

## Plugin Management Editing

- use `gf` while hovering over file path to "go to file"
- setting files are listed in plugin rc for ease of use
- `gf` opens the file under cursor in another buffer
