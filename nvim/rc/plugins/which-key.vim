Plug 'folke/which-key.nvim'

function WhichKeySetup()
lua << EOF
require("which-key").setup {}
-- local wk = require("which-key")

EOF
endfunction

augroup WhichKeySetup
    autocmd!
    autocmd User PlugLoaded call WhichKeySetup()
augroup END
