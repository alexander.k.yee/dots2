Plug 'prettier/vim-prettier', {
  \ 'do': 'yarn install',
  \ 'for': [
  \          'javascript',
  \          'javascriptreact',
  \          'typescript',
  \          'typescriptreact',
  \          'css',
  \          'less',
  \          'scss',
  \          'json',
  \          'graphql',
  \          'markdown',
  \          'vue',
  \          'yaml',
  \          'html'] }
