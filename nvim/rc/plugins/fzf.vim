Plug 'junegunn/fzf'
Plug 'junegunn/fzf.vim'
" http://owen.cymru/fzf-ripgrep-navigate-with-bash-faster-than-ever-before/
" define the backend func to search through files using rg
let g:rg_command = '
  \ rg --column --line-number --no-heading --fixed-strings --ignore-case --no-ignore --hidden --follow --color "always"
  \ -g "*.{js,json,php,md,styl,jade,html,config,py,cpp,c,go,hs,rb,conf}"
  \ -g "!{.git,node_modules,vendor}/*" '

" creates a new command :F to grep through files using rg
command! -bang -nargs=* F call fzf#vim#grep(g:rg_command .shellescape(<q-args>), 1, <bang>0)

" define custom logic for FZF / Files w/ctrl-p trigger
nnoremap <C-p> :call fzf#run( {'source':'python ~/.config/nvim/tools/FzfLister.py $(pwd)', 'sink' : 'e'})<CR>
nnoremap <Leader>b :Buffers<CR>

nnoremap <Leader>gd :call fzf#vim#tags(expand('<cword>'))<CR>
