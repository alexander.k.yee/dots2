" ------------------------------
"  Vim (Options) Global Settings
" ------------------------------

set hidden
set directory-=.

" ---------------------------------
"  Editting Settings
" ---------------------------------

set smarttab
set expandtab
set shiftwidth=4
set shiftround
set autoindent smartindent
set relativenumber
set number
set ruler
set termguicolors
set mouse=a
set confirm

" Long line wrapping
set linebreak
set showbreak=\
set breakat=\ \	;:,!?
" Wrap conditions.
set whichwrap+=h,l,<,>,[,],b,s,~
if exists('+breakindent')
  set breakindent
  set wrap
else
  set nowrap
endif

" show tab indents and trailing whitespace
set list
set listchars=tab:▸\ ,trail:·

"" ---------------------------------
""  Tagbar Settings
"" ---------------------------------
"let g:tagbar_type_typescript = {                                                  
"  \ 'ctagsbin' : 'tstags',                                                        
"  \ 'ctagsargs' : '-f-',                                                           
"  \ 'kinds': [                                                                     
"    \ 'e:enums:0:1',                                                               
"    \ 'f:function:0:1',                                                            
"    \ 't:typealias:0:1',                                                           
"    \ 'M:Module:0:1',                                                              
"    \ 'I:import:0:1',                                                              
"    \ 'i:interface:0:1',                                                           
"    \ 'C:class:0:1',                                                               
"    \ 'm:method:0:1',                                                              
"    \ 'p:property:0:1',                                                            
"    \ 'v:variable:0:1',                                                            
"    \ 'c:const:0:1',                                                              
"  \ ],                                                                            
"  \ 'sort' : 0                                                                    
"\ }
"
"let loaded_gzip = 1
