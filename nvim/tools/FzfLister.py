import os, sys, re
# import pdb
# projects you are actively working on
# add workspace package name that you want the lister to look through
# forces a limited search of workspace files to make plugin faster
PROJECTLIST = []
pwd = sys.argv[1]

# Assumes that only src dir from calling path is inside ws
# important for limiting results to only the current workspace's files
pathMatch = re.search('workspaces', pwd)
rootPath = None
if pathMatch:
    srcMatch = re.search('src', pwd)
    # grab path up to (exclusive) to src & add /
    rootPath = path[:srcMatch.end()] + '/'
    path = []
    for i in PROJECTLIST:
        path.append(i)
    path = " ".join(path)
else:
    import subprocess
    try:
        csvRoot = subprocess.check_output(
            ['git', 'rev-parse', '--show-toplevel'])
    except subprocess.CalledProcessError:
        path = ""
    else:
        path = csvRoot
## can replace with grep or find ( need to return list of files )
# generates a list of full paths to files in specified packages only
# ctrlp greps through this list to find the specified file
#cmd = 'ag -l -g "" ' + path
#cmd = 'rg --files -g "" ' + path
# must decode to str for py3 compat
path = path.decode('utf-8')
if rootPath is not None:
    os.chdir(rootPath)
# currently want until root found not only from current dir
cmd = 'fd . ' + path
os.system(cmd)
