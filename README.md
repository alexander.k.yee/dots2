# My Dots Configurations

- zsh
- nvim
- general fedora system utils

## Quickstart

- must have `ansible` installed first
- use ansible to install dots

```bash
# start from project root
cd ansible

# will require sudo access to install tools like nvim
# this command will install ALL dots
ansible-playbook -K -i inventories/local playbooks/devTools.yml
```

Designed for system to start i3 with `startx`

## Debug / Common Problems

### Change location of dots dir

- if you initially cloned the dir to /pathA then move it to /pathB
- lots of symlinks will break

**Fix**

- clone/move to your newest /pathB location then rerun the ansible playbook
- this will recreate the symlinks correctly

# TODO

- universal ctags install
- track down pkgs you need on first install
- define ansible roles for X11, dunst, i3, alacritty

# Configuration

## Wallpaper

1. create a file called `$HOME/.dotconfig` with variable `WALLPAPER=PATH/TO/IMG`
2. Let `xinitrc` trigger `feh` to set the wallpaper

## Terminal Fonts

1. modify the `.config/alacritty/alacritty.yml` fonts parameter
