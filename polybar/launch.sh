#!/usr/bin/env bash

# Terminate already running bar instances
killall -q polybar

# Wait until the processes have been shut down
while pgrep -u $UID -x polybar >/dev/null; do sleep 1; done

#for m in $(polybar --list-monitors | cut -d":" -f1); do
#	WIRELESS=$(ls /sys/class/net/ | grep ^wl | awk 'NR==1{print $1}') MONITOR=$m polybar --reload example &
#done
echo "---" | tee -a /tmp/polybar1.log 
polybar --reload topbar 2>&1 | tee -a /tmp/polybar1.log & disown

echo "Bars launched..."

